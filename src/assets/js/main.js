$(document).on('ready', function() {
    $(".variable").slick({
        infinite: true,
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: true,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    infinite: true,
                    lazyLoad: 'ondemand',
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: true,
                }
            },
            {
                breakpoint: 575,
                settings: {
                    infinite: true,
                    lazyLoad: 'ondemand',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                }
            }
        ]
    });

    $("#s-rol").on("click", function (event) {
        scrollPage (event, this)
    });
    $("#s-step").on("click", function (event) {
        scrollPage (event, this)
    });
    $('#come').on("click", function (event) {
        scrollPage (event, this)
    });
    $("#s-reg").on("click", function (event) {
        scrollPage (event, this)
    });
    $("#s-team").on("click", function (event) {
        scrollPage (event, this)
    });
    function scrollPage (event,obj) {
        event.preventDefault();
        var id  = $(obj).attr('href'),
            top = $(id).offset().top;
        top -= 10
        $('.glob').animate({scrollTop: top}, 900);
    }
});

$('#burger').on( 'click', _ => {
    $('.h-m__body').toggleClass('h-m__show')
});
$('#burger-cross').on( 'click', _ => {
    $('.h-m__body').removeClass('h-m__show')
});

$(document).on('click', function(event) {
    if (!$(event.target).closest('#burger').length) {
        $('.h-m__body').removeClass('h-m__show')
    }
})

var isSend = false

$('#send-btn').on( 'click', _ => preludeSend ());

function preludeSend () {
    if (isSend !== true && erCheck()) {
        mailApi(formDataHelper())
            .done(_ => orderSuccess())
            .fail(function(error) {
            alert('Какая то ошибка... Тогда обратитесь по почте, которая находится в футере. Ошибка: ' + JSON.stringify(error));
        });
    }
}

orderSuccess = () => {
    isSend = true
    $('.er-box').html('')
    $('#input-name').val('')
    $('#input-email').val('')
    $('#input-phone').val('')
    $('.shape-succ__text').addClass('shape-succ__show')
    $('.shape-button').addClass('shape-button_enable')
}

erCheck = () => {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    $('.er-box').html('')
    var er = false
    if ($('#input-name').val() === '') {
        $('#er-name').html('Введите имя')
        er = true
    }
    if ($('#input-phone').val() === '') {
        $('#er-phone').html('Введите телефон')
        er = true
    }
    if ($('#input-email').val() === '') {
        $('#er-email').html('Введите email')
        er = true
    }
    else if (!reg.test($('#input-email').val())) {
        $('#er-email').html('Введите корректный email')
        er = true
    }
    return !er
}

formDataHelper = () => {
    return {
        service_id: 'gmail',
        template_id: 'main',
        user_id: 'user_KoUiXo6oIsFjw0TGaHtTC',
        template_params: {
            'name': $('#input-name').val(),
            'email': $('#input-email').val(),
            'phone': $('#input-phone').val()
        }
    };
}

mailApi = (data) => $.ajax('https://api.emailjs.com/api/v1.0/email/send', {
    type: 'POST',
    data: JSON.stringify(data),
    contentType: 'application/json'
})

